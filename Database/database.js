const { Sequelize } = require("sequelize");
const {connectionString,port} = require('../Config/config')

const sequelize = new Sequelize(connectionString)

const connect = async (app) => {
    try {
        await sequelize.authenticate();
        app.listen(port || 3000)
        await sequelize.sync({force: false})
      } catch (error) {
        //console.error('Unable to connect to the database:', error);
    }

}

module.exports = {
    connect,
    sequelize
}



