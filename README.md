
# Configuration

1. .env.example is a blueprint for all configration needed for the project
2. edit .env and depends on your environment


# Development Mode

1. Clone the repo
2. `npm i`
3. `npm run dev`
4. hit the `localhost:3000` to get `Hello, World!` response
  




